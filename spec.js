// spec.js
describe('User CRUD', function() {
  it('should create user', function() {
    browser.get(browser.baseUrl + '/users/new');
    element(by.model('user.last_name')).sendKeys('init user');
    element(by.model('user.first_name')).sendKeys('init user');
    element(by.model('user.age')).sendKeys(1);
    element(by.model('user.address.country')).sendKeys('taiwan');
    element(by.model('user.address.address_1')).sendKeys('taipei')
    element(by.model('user.address.address_2')).sendKeys('tainan')

    element(by.css("input[type=submit]")).click().then(function() {
      expect(element(by.id('last_name')).getText()).toEqual('init user');
      expect(element(by.id('address_country')).getText()).toEqual('taiwan');
      expect(element(by.id('address_address_1')).getText()).toEqual('taipei');
      expect(element(by.id('address_address_2')).getText()).toEqual('tainan');
    });
  });

  it('should update user', function() {
    browser.get(browser.baseUrl + '/users');
    element.all(by.buttonText('Edit')).last().click().then(function() {
      EC = protractor.ExpectedConditions;

      var until = element.all(by.id('userForm')).last();
      browser.wait(EC.visibilityOf(until), 50000);

      element.all(by.model('user.last_name')).last().clear().sendKeys('test');
      element.all(by.css("input[type=submit]")).last().click().then(function() {
        browser.sleep(300);
        expect(element.all(by.css('td.last_name')).last().getText()).toEqual('test');
      });
    });
  });

  it('should destroy user', function() {
      browser.get(browser.baseUrl + '/users');
      var userCountElement = element(by.id('user-count'));
      var expextedUserCount = 0;

      userCountElement.getText().then(function (text) {
        expextedUserCount = text - 1;
      });

      element.all(by.buttonText('Destroy')).last().click().then(function() {
        browser.switchTo().alert().accept().then(function() {
          expect(userCountElement.getText()).toEqual(expextedUserCount.toString());
        });
      })
  });
});
