Rails.application.routes.draw do
  resources :users do
    resource :shop
    get 'count', to: 'users#user_count', on: :collection, defaults: { format: 'json' }
    get 'name', to: 'users#user_name', on: :member, defaults: { format: 'json' }
  end
end
