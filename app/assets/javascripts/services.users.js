onboardapp.factory('UserService', ['$http', '$q', function ($http, $q) {
  return {
    getUsers: function() {
      return $http.get('/users.json')
        .then(function(response) {
          return response.data;
        }, function(response) {
          return $q.reject(response.data);
        });
    },
    getUser: function(userId) {
      return $http.get('/users/' + userId + '.json')
          .then(function(response) {
            return response.data;
          }, function(response) {
            return $q.reject(response.data);
          });
    },
    updateUser: function(userId, user) {
      return $http.patch('/users/' + userId + '.json', user)
          .then(function(response) {
            return response.data;
          }, function(response) {
            return $q.reject(response.data);
          });
    },
    createUser: function(user) {
      return $http.post('/users.json', user)
          .then(function(response) {
            return response.data;
          }, function(response) {
            return $q.reject(response.data);
          });
    },
    deleteUser: function(userId) {
      return $http.delete('/users/' + userId + '.json')
          .then(function(response) {
            return response.data;
          }, function(response) {
            return $q.reject(response.data);
          });
    },
    getUserCount: function() {
      return $http.get('/users/count.json')
          .then(function(response) {
            return response.data;
          }, function(response) {
            return $q.reject(response.data);
          });
    }
  };
}]);