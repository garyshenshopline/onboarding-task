onboardapp = angular.module('onboardapp',['ui.bootstrap']);

onboardapp.config(['$httpProvider', function($httpProvider) {
  $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
}]);

