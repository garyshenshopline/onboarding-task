onboardapp.controller('UserIndexController', ['$scope', 'UserService', '$uibModal', 'UserService',
  function($scope, UserService, $uibModal, UserService) {
    $scope.users = [];
    function init() {
      getUsers();
    };
    function getUsers() {
      UserService.getUsers()
        .then(function(data) {
          $scope.users = data;
        }, function(error) {
          console.log(error);
        });
    }
    init();

    function modalPopup() {
      return $scope.modalInstance = $uibModal.open({
        template: '<user-form></user-form>',
        scope: $scope
      });
    };

    $scope.openEditUserForm = function(user) {
      $scope.selectedUser = user;
      modalPopup().result
        .then(function (data) {
          $scope.handleSuccess(data);
        })
        .then(null, function (reason) {
          $scope.handleDismiss(reason);
        });
    }

    $scope.handleSuccess = function (data) {
      console.log('Modal closed: ' + data);
    };

    $scope.handleDismiss = function (reason) {
      console.log('Modal dismissed: ' + reason);
    }

    $scope.deleteUser = function(userId) {
      UserService.deleteUser(userId)
        .then(function(data) {
          $scope.users.splice(userId, 1);
        }, function(error) {
          console.log(error);
        })
    }
}]);