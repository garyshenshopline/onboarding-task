onboardapp.directive('confirmClick', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.confirmAlertMessage;
                if (message && confirm(message)) {
                    scope.$apply(attrs.confirmClick);
                }
            });
        }
    }
}]);