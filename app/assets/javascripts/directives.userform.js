onboardapp.directive('userForm', ['UserService', '$window', '$filter',
  function(UserService, $window, $filter) {
    return {
        restrict : 'EA',
        templateUrl : '../../users/form.html',
        link: function (scope, element, attrs) {
          scope.genders = ['male', 'female', 'others'];
          scope.user = angular.copy(scope.selectedUser);
          var isNewUser = false;
          if (scope.user == undefined) {
            isNewUser = true;
            scope.user = {};
            scope.user.address = {};
            scope.user.gender = 'male';
          }

          scope.submit = function() {
            if(isNewUser) {
              UserService.createUser(scope.user)
              .then(function(data){
                $window.location = '/users/' + data.id.$oid;
              }, function(error) {
                console.log(error);
              });
            } else {
              UserService.updateUser(scope.user.id.$oid, scope.user)
              .then(function(data){
                console.log(data);
                $window.location = '/users/';
              }, function(error) {
                console.log(error);
              });
            }
          };
        }
    };
}]);