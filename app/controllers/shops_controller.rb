class ShopsController < ApplicationController
  before_action :set_user
  before_action :set_shop, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    @shop = @user.build_shop
  end

  def edit
  end

  def create
    @shop = @user.build_shop(shop_params)
    respond_to do |format|
      if @shop.save
        format.html { redirect_to user_shop_url(@user), notice: 'Shop was successfully created.' }
        format.json { render :show, status: :created, location: user_shop_url(@user) }
      else
        format.html { render :new }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @shop.update(shop_params)
        format.html { redirect_to user_shop_url(@user), notice: 'Shop was successfully updated.' }
        format.json { render :show, status: :ok, location: user_shop_url(@user) }
      else
        format.html { render :edit }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @shop.destroy
    respond_to do |format|
      format.html { redirect_to user_shop_url(@user), notice: 'Shop was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_shop
    @shop = @user.shop
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def shop_params
    params.require(:shop).permit(:shop_name)
  end
end
