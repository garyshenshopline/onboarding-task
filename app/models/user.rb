class User
  include Mongoid::Document
  include Mongoid::Timestamps
  field :first_name, type: String
  field :last_name, type: String
  field :gender, type: String
  field :age, type: Integer
  field :address, type: Hash

  # relations
  has_one :shop

  # validations
  validates_presence_of :first_name, :last_name
  validates_inclusion_of :gender, in: %w(male female others)
  validates_inclusion_of :age, in: 1..99

  def full_name
    "#{first_name} #{last_name}"
  end
end
