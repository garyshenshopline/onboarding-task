class AddCountryAndAddressToUser < ActiveRecord::Migration
  def change
    add_column :users, :country, :string
    add_column :users, :address_1, :string
    add_column :users, :address_2, :string
  end
end
